const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports = function(app) {
  app.use(
    '/api',
    createProxyMiddleware({
      target: 'https://demo-shop-phone-be.onrender.com',
      changeOrigin: true,
    })
  );
};